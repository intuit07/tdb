{
    'name': "Inverted Selection",
    'summary': """
        This module adds the possibility to add ingredients and nutritional values to the product.
         Furthermore, it can display both in the webshop in the product description.""",
    'author': "twio.tech AG",
    'website': "https://www.twio.tech",
    'category': 'Product',
    'license': 'OPL-1',
    'version': '0.1',
    'depends': [
        'stock',
        'mail',
        'uom',
        'website_sale',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/products_views.xml',
        'views/ingredients_views.xml',
        'views/ingredients_tags.xml',
        'views/nutritional_views.xml',
        'views/nutritional_tags.xml',
        'views/menuitem.xml',
        'views/product_template.xml',
        'views/settings.xml',
    ],
}
