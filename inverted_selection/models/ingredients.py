from odoo import models, fields, api


class IngredientsProduct(models.Model):
    _name = "ingredients.product"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Ingredients Nutritional"

    name = fields.Char(string="Name", tracking=True, translate=True)
    country_id = fields.Many2one("res.country", string="Country", tracking=True)
    tags_ids = fields.Many2many("ingredient.tags", string="Tags")
    tags = fields.Char(tracking=True)

    @api.onchange("tags_ids")
    def onchange_tags(self):
        for tag in self:
            tag.tags = ""
            for tags in self.tags_ids:
                tag.tags += f"{tags.name}, "
