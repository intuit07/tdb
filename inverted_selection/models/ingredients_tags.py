from odoo import models, fields, api


class IngredientTags(models.Model):
    _name = "ingredient.tags"
    _description = "Ingredient Tags"

    name = fields.Char(string="Tag name")
