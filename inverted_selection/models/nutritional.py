from odoo import models, fields, api


class NutritionalProduct(models.Model):
    _name = "nutritional.product"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Nutritional"

    name = fields.Char(string="Name", tracking=True, translate=True)
    nutritional_country_id = fields.Many2one("res.country", string="Country", tracking=True)
    nutritional_tags_ids = fields.Many2many("nutritional.tags", string="Tags")
    tags = fields.Char(tracking=True)
    sequence = fields.Integer(string="Sequence")
    units = fields.Many2one('uom.uom', string="Units", required=True)

    @api.onchange("nutritional_tags_ids")
    def onchange_nutritional_tags(self):
        for tag in self:
            tag.tags = ""
            for tags in self.nutritional_tags_ids:
                tag.tags += f"{tags.name}, "
