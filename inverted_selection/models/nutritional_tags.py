from odoo import models, fields, api


class NutritionalTags(models.Model):
    _name = "nutritional.tags"
    _description = "Nutritional Tags"

    name = fields.Char(string="Tag name")
