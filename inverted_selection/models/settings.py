from odoo import models, fields, api


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    product_ingredient = fields.Boolean("Ingredients")
    product_nutritional = fields.Boolean("Nutritional")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res['product_nutritional'] = self.env['ir.config_parameter'].sudo().get_param('inverted_selection.product_nutritional')
        res['product_ingredient'] = self.env['ir.config_parameter'].sudo().get_param('inverted_selection.product_ingredient')
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('inverted_selection.product_nutritional',
                                                         self.product_nutritional)
        self.env['ir.config_parameter'].sudo().set_param('inverted_selection.product_ingredient',
                                                         self.product_ingredient)
