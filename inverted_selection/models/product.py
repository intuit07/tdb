from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = "product.template"

    ingredient_line_ids = fields.One2many("product.template.ingredient", "product_ingredient_id")
    nutritional_line_ids = fields.One2many("product.template.nutritional", "product_nutritional_id")
    weight_per_serving = fields.Float(string="Weight per Serving")
    standard_value = fields.Float(string="Standard Value", default=100, store=True)

    def get_coefficient(self, w, s, value, unit):
        if value:
            if w and s:
                coeff = w / s
                return f"{round(coeff * value)} {unit}"
        else:
            return f"0 {unit}"

    def check_ingredient(self):
        return self.env['ir.config_parameter'].sudo().get_param('inverted_selection.product_ingredient')

    def check_nutritional(self):
        return self.env['ir.config_parameter'].sudo().get_param('inverted_selection.product_nutritional')


class ProductTemplateIngredient(models.Model):
    _name = "product.template.ingredient"
    _description = "Ingredients Product Template"

    ingredient_id = fields.Many2one("ingredients.product", string="Name")
    country_id = fields.Many2one("res.country", string="Country", related="ingredient_id.country_id")
    tags_ids = fields.Many2many("ingredient.tags", string="Tags", related="ingredient_id.tags_ids")
    product_ingredient_id = fields.Many2one("product.template")
    value = fields.Char(string="Value")


class ProductTemplateNutritional(models.Model):
    _name = "product.template.nutritional"
    _description = "Nutritional Product Template"

    nutritional_id = fields.Many2one("nutritional.product", string="Name")
    nutritional_country_id = fields.Many2one("res.country", string="Country",
                                             related="nutritional_id.nutritional_country_id")
    nutritional_tags_ids = fields.Many2many("nutritional.tags", string="Tags",
                                            related="nutritional_id.nutritional_tags_ids")
    product_nutritional_id = fields.Many2one("product.template")
    nutritional_value = fields.Integer(string="Value")

    def check_nutritional_value(self, value, unit):
        if not value:
            return f"0 {unit}"
        else:
            return f"{value} {unit}"